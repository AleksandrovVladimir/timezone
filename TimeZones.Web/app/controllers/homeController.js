﻿'use strict';
app.controller('homeController', ['$scope', '$rootScope', 'authService',
    function ($scope, $rootScope, authService) {

        $scope.errorMessage = $rootScope.errorMessage;
        $rootScope.errorMessage = '';
        $scope.authentication = authService.authentication;

        $scope.rolesString = function() {
            if ($rootScope.role) {
                return "Your Role is " + $rootScope.role;

            }
            return "";
        }

        if ($rootScope.role) {
            $rootScope.isAdmin = $rootScope.role === "Admin";
            $rootScope.isUserManager = $rootScope.role === "UserManager";
            $rootScope.isUser = $rootScope.role === "User";

            $rootScope.canManageUsers = $scope.isAdmin || $scope.isUserManager;
            $rootScope.canManageData = $scope.isAdmin;
        }
        
    }]);