﻿'use strict';
app.controller('indexController', [
    '$scope', '$location', 'authService', '$rootScope', '$filter', 'userManagerService', function ($scope, $location, authService, $rootScope, $filter, userManagerService) {

        $scope.logOut = function () {
            authService.logOut();
            $location.path('/home');
        }
        
        $scope.authentication = authService.authentication;

        function init() {
            if ($scope.authentication.userName) {
                userManagerService.getRoles($scope.authentication.userName).then(function (roleResponse) {
                    $rootScope.role = roleResponse.data.role;

                    if ($rootScope.role) {
                        $rootScope.isAdmin = $rootScope.role === "Admin";
                        $rootScope.isUserManager = $rootScope.role === "UserManager";
                        $rootScope.isUser = $rootScope.role === "User";

                        $rootScope.canManageUsers = $scope.isAdmin || $scope.isUserManager;
                        $rootScope.canManageData = $scope.isAdmin;
                    }
                });
            }

           
        }

        init();

    }]);