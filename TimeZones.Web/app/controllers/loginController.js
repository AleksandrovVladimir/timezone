﻿'use strict';
app.controller('loginController', [
    '$scope', '$location', 'authService', 'userManagerService', '$rootScope', '$q', function ($scope, $location, authService, userManagerService, $rootScope, $q) {

    $scope.loginData = {
        userName: "",
        password: ""
    };

    $scope.message = "";

    $scope.login = function () {

        $q.all([authService.login($scope.loginData), userManagerService.getRoles($scope.loginData.userName)])
            .then(function(response) {
                $rootScope.role = response[1].data.role;
                $location.path('/home');
            },

            function (err) {
                 $scope.message = err.error_description;
            });
        };

}]);