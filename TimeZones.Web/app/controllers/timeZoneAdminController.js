﻿'use strict';
app.controller('timeZoneAdminController', [
    '$scope', 'dataService', 'authService', 'userManagerService', '$rootScope', '$location', '$filter',
    function ($scope, dataService, authService, userManagerService, $rootScope, $location, $filter) {

        $scope.timeZones = [];

        $scope.allTimeZones = [];
        dataService.getTimeZoneList().then(function (response) {
            $scope.allTimeZones = response.data;
        });

        var loadUsers = function () {
            userManagerService.getUsers().then(function (results) {
                $scope.users = $filter('filter')(results.data, "User", true);                
            }, function (error) {
                $rootScope.errorMessage = error.data.message;
                $location.path('/home');
            });
        }

        loadUsers();

        $scope.userChanged = function () {
            if ($scope.user != '') {
                loadData($scope.user);
            } else {
                $scope.timeZones = [];
            }
        }

        function loadData(userName) {
            dataService.getAdminData(userName).then(function (results) {
                $scope.timeZones = results.data;                
            }, function (error) {                
            });
        }

        $scope.deleteTimeZone = function (index, timeZone) {
            dataService.deleteTimeZoneAdmin(timeZone, $scope.user).then(function(response) {
                $scope.timeZones.splice(index, 1);
            });
        }

        $scope.showAddTimeZone = false;
        $scope.newTimeZone = {
            name: '',
            locations: '',
            offset: ''
        }

        $scope.toggleShowAddTimeZone = function () {
            if ($scope.user) {
                $scope.showAddTimeZone = !$scope.showAddTimeZone;

                $scope.newTimeZone = {
                    name: '',
                    locations: '',
                    offset: ''
                }
            }
        }
       
        $scope.timeZoneChanged = function (timeZoneId) {
            $scope.timeZoneId = timeZoneId;

            var systemTimeZone = $filter('filter')($scope.allTimeZones, timeZoneId, true);

            $scope.newTimeZone.name = systemTimeZone[0].name;
            $scope.newTimeZone.locations = systemTimeZone[0].locations;
            $scope.newTimeZone.offset = systemTimeZone[0].offset;
        }

        $scope.addTimeZone = function () {
            dataService.addTimeZoneAdmin($scope.newTimeZone, $scope.user).then(function (response) {
                $scope.toggleShowAddTimeZone();
                loadData($scope.user);
            });
        }

        var timeZonesCopy = [];
        $scope.editTimeZone = function (timeZone) {
            timeZone.editMode = !timeZone.editMode;
            if (timeZone.editMode) {
                var copy = {
                    editMode: timeZone.editMode,
                    id: timeZone.id,
                    name: timeZone.name,
                    locations: timeZone.locations,
                    offset: timeZone.offset
                };
                timeZonesCopy.push(copy);
            } else {
                var copy = $filter('filter')(timeZonesCopy, timeZone.id, true)[0];
                timeZone.editMode = !copy.editMode;
                timeZone.name = copy.name;
                timeZone.locations = copy.locations;
                timeZone.offset = copy.offset;
            }
        }

        $scope.updateTimeZone = function (timeZone) {
            dataService.updateTimeZoneAdmin(timeZone, $scope.user).then(function (response) {
                loadData($scope.user);
                timeZonesCopy = [];
            });
        }
    }
]);