﻿'use strict';
app.controller('timeZoneController', ['$scope', '$rootScope', 'authService', '$filter', 'dataService',
    function ($scope, $rootScope, authService, $filter, dataService) {
        $scope.allTimeZones = [];
        dataService.getTimeZoneList().then(function (response) {
            $scope.allTimeZones = response.data;
        });


        $scope.timeZones = [];

        function loadData() {
            dataService.getData().then(function (results) {
                $scope.timeZones = results.data;                
            }, function (error) {                
            });
        }

        loadData();

        $scope.deleteTimeZone = function (index, timeZone) {
            dataService.deleteTimeZone(timeZone).then(function (response) {
                $scope.timeZones.splice(index, 1);
            });
        }

        $scope.showAddTimeZone = false;
        $scope.newTimeZone = {
            name: '',
            locations: '',
            offset: ''
        }

        $scope.toggleShowAddTimeZone = function () {
            $scope.showAddTimeZone = !$scope.showAddTimeZone;

            $scope.newTimeZone = {
                name: '',
                locations: '',
                offset: ''
            }
        }

        $scope.timeZoneChanged = function (timeZoneId) {
            $scope.timeZoneId = timeZoneId;
            
            var systemTimeZone = $filter('filter')($scope.allTimeZones, timeZoneId, true);

            $scope.newTimeZone.name = systemTimeZone[0].name;
            $scope.newTimeZone.locations = systemTimeZone[0].locations;
            $scope.newTimeZone.offset = systemTimeZone[0].offset;            
        }

        $scope.addTimeZone = function () {
            dataService.addTimeZone($scope.newTimeZone).then(function(response) {
                $scope.toggleShowAddTimeZone();
                loadData();
            });
        }

        var timeZonesCopy = [];
        $scope.editTimeZone = function (timeZone) {
            timeZone.editMode = !timeZone.editMode;
            if (timeZone.editMode) {
                var copy = {                    
                    editMode: timeZone.editMode,
                    id: timeZone.id,
                    name: timeZone.name,
                    locations: timeZone.locations,
                    offset: timeZone.offset
                };
                timeZonesCopy.push(copy);
            } else {
                var copy = $filter('filter')(timeZonesCopy, timeZone.id, true)[0];                
                timeZone.editMode = !copy.editMode;
                timeZone.name = copy.name;
                timeZone.locations = copy.locations;
                timeZone.offset = copy.offset;
            }
        }

        $scope.updateTimeZone = function(timeZone) {
            dataService.updateTimeZone(timeZone).then(function (response) {                
                loadData();
                timeZonesCopy = [];
            });
        }
    }]);