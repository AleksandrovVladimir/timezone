﻿'use strict';
app.controller('userManagerController', ['$scope', 'userManagerService', '$location', '$rootScope', 'authService',
    function ($scope, userManagerService, $location, $rootScope, authService) {

        $scope.users = [];
        $scope.curentUserName = authService.authentication.userName;

    var loadUsers = function () {
        userManagerService.getUsers().then(function (results) {

            $scope.users = results.data;

        }, function (error) {
            $rootScope.errorMessage = error.data.message;
            $location.path('/home');
        });
    }

    loadUsers();

    $scope.deleteUserRole = function (user) {
        userManagerService.deleteUserRole(user)
            .then(function (results) {
                loadUsers();
            }, function (error) {
                $rootScope.errorMessage = error.data.message;
                $location.path('/home');
            });
    }

    $scope.userRoleChanged = function(user) {
        userManagerService.updateUserRole(user).then(function(response) {
            loadUsers();
        });
    }

}]);