﻿'use strict';
app.directive("time", function (dateFilter) {
    return function (scope, element, attrs) {
        var format;
        var timeZone;

        scope.$watch(attrs.time, function (value) {
            format = value;
            updateTime();
        });

        scope.$watch(attrs.zone, function (value) {
            timeZone = value;
            updateTime();
        });

        function updateTime() {
            var dt;
            if (timeZone) {
                dt = dateFilter(getTime(timeZone), format);
            } else {
                dt = dateFilter(new Date(), format);
            }
            element.text(dt);
        }

        function updateLater() {
            setTimeout(function () {
                updateTime();
                updateLater();
            }, 1000);
        }        

        function getTime(timeZone) {
            var offset = timeZone.offset;
            if (offset.length === 8) {
                offset = '+' + offset;
            }
            return moment.utc().utcOffset(offset).format(format);
        }
        
        updateLater();
    }
});