﻿'use strict';
app.directive('timeZone',
    function () {
        return {
            restrict: 'A',
            templateUrl: '/app/views/timeZonesTable.html',            
            controller: 'timeZoneController'
        };
    }
).directive('timeZoneAdmin',
    function () {
        return {
            restrict: 'A',
            templateUrl: '/app/views/timeZonesTable.html',
            controller: 'timeZoneAdminController'
        };
    }
)