﻿'use strict';
app.factory('dataService', ['$http', '$q', function ($http, $q) {

    var serviceBase = 'http://localhost:2896/';
    var dataServiceFactory = {};

    var _getData = function () {
        return $http.get(serviceBase + 'api/data/getData').then(function (results) {
            return results;
        });
    };

    var _getAdminData = function (userName) {
        return $http.get(serviceBase + 'api/data/getData', { params: { "userName": userName } }).then(function (results) {
            return results;
        });
    };

    var _getTimeZoneList = function () {
        return $http.get(serviceBase + 'api/data/GetTimeZoneList').then(function (results) {
            return results;
        });
    };

    var _addTimeZone = function(timeZone) {
        return $http.post(serviceBase + 'api/data/AddData', timeZone).then(function (results) {
            return results;
        },
        function errorCallback(response) {
            return $q.reject(response);
        });
    }

    var _addTimeZoneAdmin = function (timeZone, userName) {
        return $http.post(serviceBase + 'api/data/AddDataAdmin', {
            timeZone: timeZone,
            userName: userName
        }).then(function (results) {
            return results;
        },
        function errorCallback(response) {
            return $q.reject(response);
        });
    }

    var _deleteTimeZone = function (timeZone) {
        return $http.post(serviceBase + 'api/data/DeleteData', timeZone).then(function (results) {
            return results;
        },
        function errorCallback(response) {
            return $q.reject(response);
        });
    }

    var _deleteTimeZoneAdmin = function (timeZone, userName) {
        return $http.post(serviceBase + 'api/data/DeleteDataAdmin', {
            timeZone: timeZone,
            userName: userName
        }).then(function (results) {
            return results;
        },
        function errorCallback(response) {
            return $q.reject(response);
        });
    }

    var _updateTimeZone = function (timeZone) {
        return $http.post(serviceBase + 'api/data/UpdateData', timeZone).then(function (results) {
            return results;
        },
        function errorCallback(response) {
            return $q.reject(response);
        });
    }

    var _updateTimeZoneAdmin = function (timeZone, userName) {
        return $http.post(serviceBase + 'api/data/UpdateDataAdmin', {
            timeZone: timeZone,
            userName: userName
        }).then(function (results) {
            return results;
        },
        function errorCallback(response) {
            return $q.reject(response);
        });
    }

    dataServiceFactory.getData = _getData;
    dataServiceFactory.getAdminData = _getAdminData;
    dataServiceFactory.getTimeZoneList = _getTimeZoneList;
    dataServiceFactory.addTimeZone = _addTimeZone;
    dataServiceFactory.addTimeZoneAdmin = _addTimeZoneAdmin;
    dataServiceFactory.deleteTimeZone = _deleteTimeZone;
    dataServiceFactory.deleteTimeZoneAdmin = _deleteTimeZoneAdmin;
    dataServiceFactory.updateTimeZoneAdmin = _updateTimeZoneAdmin;
    dataServiceFactory.updateTimeZone = _updateTimeZone;


    return dataServiceFactory;

}]);
