﻿'use strict';
app.factory('userManagerService', ['$http', '$location', '$q', function ($http, $location, $q) {

    var serviceBase = 'http://localhost:2896/';
    var userManagerServiceFactory = {};

    var _getUsers = function () {

        return $http.get(serviceBase + 'api/UserManager/GetUsers').then(function (results) {
            return results;
        },
        function errorCallback(response) {
            return $q.reject(response);
        });
    };

    var _deleteUserRole = function (user) {

        return $http.post(serviceBase + 'api/UserManager/DeleteUserRole', user).then(function (results) {
            return results;
        },
        function errorCallback(response) {
            return $q.reject(response);
        });
    };

    var _getRoles = function (userName) {

        return $http.get(serviceBase + 'api/UserManager/GetRole/' + userName).then(function (results) {
            return results;
        },
            function errorCallback(response) {
                return $q.reject(response);
            });
    };

    var _updateUserRole = function(user) {
        return $http.post(serviceBase + 'api/UserManager/UpdateUserRole', user).then(function (results) {
            return results;
        },
        function errorCallback(response) {
            return $q.reject(response);
        });
    }

    userManagerServiceFactory.getUsers = _getUsers;
    userManagerServiceFactory.deleteUserRole = _deleteUserRole;
    userManagerServiceFactory.getRoles = _getRoles;
    userManagerServiceFactory.updateUserRole = _updateUserRole;

    return userManagerServiceFactory;

}]);