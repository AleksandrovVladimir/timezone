﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace TimeZones
{
    public class AuthContext : IdentityDbContext<IdentityUser>
    {
        public AuthContext()
            : base("AuthContext")
        {

        }
    }
}