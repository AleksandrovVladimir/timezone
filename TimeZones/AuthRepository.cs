using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using TimeZones.Models;

namespace TimeZones
{
    public class AuthRepository : IDisposable
    {
        private AuthContext _ctx;

        private UserManager<IdentityUser> _userManager;
        private RoleManager<IdentityRole> _roleManager; 

        public AuthRepository()
        {
            _ctx = new AuthContext();
            _userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(_ctx));
            _roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(_ctx));
        }

        public IdentityResult RegisterUser(UserRegistrationModel userRegistrationModel)
        {            
            var user = new IdentityUser
            {
                UserName = userRegistrationModel.UserName
            };

            if (_roleManager.FindByName(userRegistrationModel.Role.ToString()) == null)
            {
                _roleManager.Create(new IdentityRole(userRegistrationModel.Role.ToString()));
            }
            var result =_userManager.Create(user, userRegistrationModel.Password);
            _userManager.AddToRole(user.Id, userRegistrationModel.Role.ToString());
            
            return result;
        }

        public List<UserRoleModel> GetUsers()
        {
            var users = _userManager.Users.ToList();

            return users.Select(user => new UserRoleModel()
            {
                UserName = user.UserName, 
                Role = GetRoleNameByUserName(user.UserName).First()
            }).ToList();
        }

        public bool DeleteUserRole(UserRoleModel user)
        {
            var identityUser = _userManager.Users.First(u => u.UserName == user.UserName);            

            if (user.Role != null)
            {
                _userManager.RemoveFromRole(identityUser.Id, user.Role);                
            }

            if (identityUser.Roles.Count == 0)
            {
                _userManager.Delete(identityUser);
            }

            return true;
        }

        public bool UpdateUserRole(UserRoleModel user)
        {
            var identityUser = _userManager.Users.First(u => u.UserName == user.UserName);

            if (identityUser.Roles != null)
            {
                var roles = identityUser.Roles.ToList();
                foreach (var role in roles)
                {
                    _userManager.RemoveFromRole(identityUser.Id, GetRoleName(role.RoleId));
                }
            }

            _userManager.AddToRole(identityUser.Id, user.Role);           

            return true;
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            var user = await _userManager.FindAsync(userName, password);

            return user;
        }

        public string GetRoleName(string roleId)
        {
            var role = _roleManager.FindById(roleId);
            return role.Name;
        }

        public List<string> GetRoleNameByUserName(string userName)
        {            
            var identityUser = _userManager.Users.FirstOrDefault(u => u.UserName == userName);
            if (identityUser != null)
            {
                return identityUser.Roles.Select(role => GetRoleName(role.RoleId)).ToList();
            }
            return new List<string>();
        }

        public string GetUserId(string userName)
        {
            return _userManager.Users.First(u => u.UserName == userName).Id;
        }

        public void Dispose()
        {
            _ctx.Dispose();
            _userManager.Dispose();

        }
    }
}