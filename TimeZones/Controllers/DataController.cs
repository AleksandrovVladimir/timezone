﻿using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using TimeZones.Models;
using TimeZone = TimeZones.Models.TimeZoneModel;

namespace TimeZones.Controllers
{
    [RoutePrefix("api/Data")]
    public class DataController : ApiController
    {
        private readonly AuthRepository _authRepository = null;

        public DataController()
        {
            _authRepository = new AuthRepository();
        }

        [Authorize(Roles = "Admin")]        
        [Route("GetData")]        
        public IHttpActionResult GetData(string userName)
        {
            
            var userId = _authRepository.GetUserId(userName);
            var res = DataRepository.GeTimeZoneModels(userId)
                .Select(tz => tz.TimeZone).ToList();            

            return Ok(res);
        }

        [Authorize(Roles = "User")]
        [Route("GetData")]
        public IHttpActionResult GetData()
        {
            var user = User.Identity.GetUserName();
            var userId = _authRepository.GetUserId(user);
            var res = DataRepository.GeTimeZoneModels(userId)
                .Select(tz => tz.TimeZone).ToList();

            return Ok(res);
        }

        [Authorize]
        [Route("GetTimeZoneList")]
        public IHttpActionResult GetTimeZoneList()
        {
            return Ok(TimeZoneModel.CreateTimeZones());
        }

        [Authorize(Roles = "Admin")]
        [Route("AddDataAdmin")]
        [HttpPost]
        public IHttpActionResult AddDataAdmin(AssignedTimeZone assignedTimeZone)
        {
            var userId = _authRepository.GetUserId(assignedTimeZone.UserName);
            DataRepository.AddTimeZone(new AssignedTimeZoneDataModel()
            {
                UserId = userId,
                TimeZone = assignedTimeZone.TimeZone 
            });
            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [Route("DeleteDataAdmin")]
        [HttpPost]
        public IHttpActionResult DeleteDataAdmin(AssignedTimeZone timeZoneModel)
        {
            var userId = _authRepository.GetUserId(timeZoneModel.UserName);
            DataRepository.DeleteTimeZone(new AssignedTimeZoneDataModel()
            {
                UserId = userId,
                TimeZone = timeZoneModel.TimeZone              
            });
            return Ok();
        }

        [Authorize(Roles = "Admin")]
        [Route("UpdateDataAdmin")]
        [HttpPost]
        public IHttpActionResult UpdateData(AssignedTimeZone assignedTimeZone)
        {
            var userId = _authRepository.GetUserId(assignedTimeZone.UserName);
            DataRepository.UpdateTimeZone(new AssignedTimeZoneDataModel()
            {
                UserId = userId,
                TimeZone = assignedTimeZone.TimeZone
            });
            return Ok();
        }

        [Authorize(Roles = "User")]
        [Route("AddData")]
        [HttpPost]
        public IHttpActionResult AddData(TimeZoneModel timeZone)
        {
            var user = User.Identity.GetUserName();
            var userId = _authRepository.GetUserId(user);
            DataRepository.AddTimeZone(new AssignedTimeZoneDataModel()
            {
                UserId = userId,
                TimeZone = timeZone
            });
            return Ok();
        }

        [Authorize(Roles = "User")]
        [Route("UpdateData")]
        [HttpPost]
        public IHttpActionResult UpdateData(TimeZoneModel timeZone)
        {
            var user = User.Identity.GetUserName();
            var userId = _authRepository.GetUserId(user);
            DataRepository.UpdateTimeZone(new AssignedTimeZoneDataModel()
            {
                UserId = userId,
                TimeZone = timeZone
            });
            return Ok();
        }

        [Authorize(Roles = "User")]
        [Route("DeleteData")]
        [HttpPost]
        public IHttpActionResult DeleteData(TimeZoneModel timeZone)
        {
            var user = User.Identity.GetUserName();
            var userId = _authRepository.GetUserId(user);
            DataRepository.DeleteTimeZone(new AssignedTimeZoneDataModel()
            {
                UserId = userId,
                TimeZone = timeZone
            });
            return Ok();
        }
    }
}