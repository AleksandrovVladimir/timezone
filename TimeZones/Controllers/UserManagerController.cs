﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using TimeZones.Models;

namespace TimeZones.Controllers
{
    [RoutePrefix("api/UserManager")]
    public class UserManagerController : ApiController
    {
        [Authorize(Roles = "Admin, UserManager")]
        [Route("GetUsers")]
        public IHttpActionResult GetUsers()
        {
            using (var _repo = new AuthRepository())
            {
                return Ok(_repo.GetUsers());
            }
        }

        [Authorize(Roles = "Admin, UserManager")]
        [Route("DeleteUserRole")]
        [HttpPost]
        public IHttpActionResult DeleteUserRole(UserRoleModel user)
        {
            using (var _repo = new AuthRepository())
            {
                return Ok(_repo.DeleteUserRole(user));
            }
        }

        [AllowAnonymous]
        [Route("GetRole/{userName}")]
        public IHttpActionResult GetRoles([FromUri]string userName)
        {           
            using (var _repo = new AuthRepository())
            {
                var user = _repo.GetRoleNameByUserName(userName).FirstOrDefault();
                var result = new UserRoleModel()
                {
                    UserName = userName                    
                };

                if (user != null)
                {
                    result.Role = _repo.GetRoleNameByUserName(userName).First();
                }
                return Ok(result);
            }
        }

        [Authorize(Roles = "Admin, UserManager")]
        [Route("UpdateUserRole")]
        [HttpPost]
        public IHttpActionResult UpdateUserRole(UserRoleModel user)
        {
            using (var _repo = new AuthRepository())
            {
                return Ok(_repo.UpdateUserRole(user));
            }
        }
        
    }
}