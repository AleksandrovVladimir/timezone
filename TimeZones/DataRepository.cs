﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Web;
using TimeZones.Models;

namespace TimeZones
{
    public class DataRepository 
    {
        public static void AddTimeZone(AssignedTimeZoneDataModel timeZoneModel)
        {
            using (var db = new TimeZonesDataEntities())
            {
                if (timeZoneModel.TimeZone.Id == null)
                {                    
                    timeZoneModel.TimeZone.Id = Guid.NewGuid().ToString();
                }

                db.TimeZone.Add(new TimeZone()
                {
                    TimeZoneId = timeZoneModel.TimeZone.Id,
                    UserId = timeZoneModel.UserId,
                    City = timeZoneModel.TimeZone.Locations,
                    Name = timeZoneModel.TimeZone.Name,
                    Offset = timeZoneModel.TimeZone.Offset.ToString()
                });

                db.SaveChanges();
            }
        }

        public static void UpdateTimeZone(AssignedTimeZoneDataModel timeZoneModel)
        {
            using (var db = new TimeZonesDataEntities())
            {
                var updateItem = db.TimeZone.FirstOrDefault(tz => tz.UserId == timeZoneModel.UserId && tz.TimeZoneId == timeZoneModel.TimeZone.Id);
                if (updateItem == null)
                {
                    return;    
                }

                updateItem.City = timeZoneModel.TimeZone.Locations;
                updateItem.Name = timeZoneModel.TimeZone.Name;
                updateItem.Offset = timeZoneModel.TimeZone.Offset.ToString();                

                db.SaveChanges();
            }
        }

        public static List<AssignedTimeZoneDataModel> GeTimeZoneModels(string userId)
        {
            using (var db = new TimeZonesDataEntities())
            {
                var timeZones = db.TimeZone.Where(tz => tz.UserId == userId);
                var result = new List<AssignedTimeZoneDataModel>();
                foreach (var timeZone in timeZones)
                {
                    result.Add( new AssignedTimeZoneDataModel()
                    {
                        UserId = timeZone.UserId,
                        TimeZone = new TimeZoneModel()
                        {
                            Id = timeZone.TimeZoneId,
                            Locations = timeZone.City,
                            Offset = TimeSpan.Parse(timeZone.Offset),
                            Name = timeZone.Name
                        }
                    });
                }
                return result;
            }
        }

        public static void DeleteTimeZone(AssignedTimeZoneDataModel timeZone)
        {
            using (var db = new TimeZonesDataEntities())
            {
                var removeItems = db.TimeZone.Where(tz => tz.UserId == timeZone.UserId && tz.TimeZoneId == timeZone.TimeZone.Id);

                db.TimeZone.RemoveRange(removeItems);
                db.SaveChanges();
            }
        }
    }
}