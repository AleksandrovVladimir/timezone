namespace TimeZones.Models
{
    public class AssignedTimeZone
    {
        public string UserName { get; set; }
        public TimeZoneModel TimeZone { get; set; }
    }

    public class AssignedTimeZoneDataModel
    {
        public string UserId { get; set; }
        public TimeZoneModel TimeZone { get; set; }        
    }
}