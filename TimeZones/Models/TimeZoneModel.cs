using System;
using System.Collections.Generic;
using System.Linq;

namespace TimeZones.Models
{
    public class TimeZoneModel
    {
        public string Id { get; set; } 
        public string Name { get; set; }
        public string Locations { get; set; }
        public TimeSpan Offset { get; set; }

        public static List<TimeZoneModel> CreateTimeZones()
        {
            var timeZoneInfos = TimeZoneInfo.GetSystemTimeZones();

            return timeZoneInfos.Select(timeZoneInfo => new TimeZoneModel()
            {
                Id = timeZoneInfo.Id, 
                Name = timeZoneInfo.StandardName, 
                Locations = timeZoneInfo.DisplayName, 
                Offset = timeZoneInfo.BaseUtcOffset
            }).ToList();
        }
    }
}