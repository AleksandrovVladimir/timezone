﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TimeZones.Models
{
    public class UserModel
    {
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Display(Name = "Role")]
        public List<string> Roles { get; set; }
    }

    public class UserRoleModel
    {
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }
    }
}