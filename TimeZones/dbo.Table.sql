﻿USE [TimeZonesData]
GO

/****** Object:  Table [dbo].[TimeZone]    Script Date: 24.11.2015 21:51:48 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[TimeZone](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TimeZoneId] [nvarchar](50) NOT NULL,
	[UserId] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](200) NULL,
	[City] [nvarchar](200) NULL,
	[Offset] [nvarchar](50) NULL,
 CONSTRAINT [PK_TimeZone] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[TimeZone] ADD  DEFAULT ('00:00:00') FOR [Offset]
GO

